const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/scripts/script.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    filename: 'script.js',
  },
};
