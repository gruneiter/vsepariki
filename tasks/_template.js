import gulp from 'gulp';
import gulpIf from 'gulp-if';
import pug from 'gulp-pug';
import inheritance from 'gulp-pug-inheritance';
import cached from 'gulp-cached';
import filter from 'gulp-filter';
import rename from 'gulp-rename';
import plumber from 'gulp-plumber';

import { distDir } from '../gulpfile.babel';

gulp.task('template', (done) => {
  gulp.src('./src/pages/**/*.pug')
    .pipe(plumber())
    .pipe(cached('template'))
    .pipe(gulpIf(global.isWatching, inheritance({ basedir: 'src', skip: 'node_modules' })))
    .pipe(filter((file) => /src[\\/]pages/.test(file.path)))
    .pipe(pug({
      basedir: 'src',
      pretty: true,
    }))
    .pipe(rename((path) => {
      const filePath = path;
      filePath.dirname = filePath.dirname.startsWith('pages') ? `.${filePath.dirname.slice(5)}` : filePath.dirname;
    }))
    .pipe(gulp.dest(distDir));
  done();
});
