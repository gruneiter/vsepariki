import gulp from 'gulp';
import changed from 'gulp-changed';

import { distDir, templateDir } from '../gulpfile.babel';

gulp.task('copy:resources', (done) => {
  gulp.src(['src/resources/**'])
    .pipe(changed(distDir))
    .pipe(gulp.dest(distDir));
  done();
});

gulp.task('copy:template', (done) => {
  gulp.src('src/template/**')
    .pipe(changed(`${templateDir}`))
    .pipe(gulp.dest(`${templateDir}`));
  done();
});

gulp.task('copy', gulp.series('copy:resources', 'copy:template'));
