


var homeCatalogSwitcher = document.getElementById('home-catalog__mobile-switch')
var homeCatalogTabActive = document.getElementsByClassName('home-catalog__tab_active')

var homeCatalogTabs = document.querySelectorAll('.home-catalog__tab')
var homeCatalogTabsBlock = document.querySelectorAll('.home-catalog__tabs')
var productTabs = document.querySelectorAll('.product-tabs__tab')

if (homeCatalogSwitcher && homeCatalogTabActive.length) {

  homeCatalogSwitcher.innerText = homeCatalogTabActive[0].innerText
  function homeCatalogTabSwitch() {
    var homeCatalogTabsContents = document.querySelectorAll('.home-catalog__tab-content')
    var currentTabName = this.innerText;
    for (var j = 0; j < homeCatalogTabsContents.length; j++) {
      homeCatalogTabsContents[j].classList.remove('home-catalog__tab-content_active');
      homeCatalogTabs[j].classList.remove('home-catalog__tab_active')
      this.classList.add('home-catalog__tab_active')
      this.parentElement.classList.remove('home-catalog__tabs_m-active')
      homeCatalogSwitcher.innerText = currentTabName
      if ( this.dataset.homeCatalogTab === homeCatalogTabsContents[j].dataset.homeCatalogTab ) {
        homeCatalogTabsContents[j].classList.add('home-catalog__tab-content_active')
      }
    }
  }
  homeCatalogSwitcher.addEventListener('click',homeCatalogSwitch)
  function homeCatalogSwitch() {
    if (!homeCatalogTabsBlock[0].classList.contains('home-catalog__tabs_m-active')){
      homeCatalogTabsBlock[0].classList.add('home-catalog__tabs_m-active')
      homeCatalogSwitcher.classList.add('home-catalog__mobile-switch_active')
    } else {
      homeCatalogTabsBlock[0].classList.remove('home-catalog__tabs_m-active')
      homeCatalogSwitcher.classList.remove('home-catalog__mobile-switch_active')
    }
  }
  for ( var i = 0; i < homeCatalogTabs.length; i++ ) {
    homeCatalogTabs[i].addEventListener ('click', homeCatalogTabSwitch)
  }
}
if (productTabs) {
  function productTabSwitch(){
    var productTabsContents = document.querySelectorAll('.product-tabs__tab-content')
    for ( var k = 0; k < productTabs.length; k++ ) {
      productTabs[k].classList.remove('product-tabs__tab_active')
    }
    this.classList.add('product-tabs__tab_active')
    for (var j = 0; j < productTabsContents.length; j++) {

      productTabsContents[j].classList.remove('product-tabs__tab-content_active');
      if ( this.dataset.productTab === productTabsContents[j].dataset.productTab ) {
        productTabsContents[j].classList.add('product-tabs__tab-content_active')
      }
    }
    for (var j = 0; j < productTabs.length; j++) {
      if ( this.dataset.productTab === productTabs[j].dataset.productTab && this !== productTabs[j] ) {
        productTabs[j].classList.add('product-tabs__tab_active')
      }
    }
  }
  for ( var i = 0; i < productTabs.length; i++ ) {
    productTabs[i].addEventListener ('click', productTabSwitch)
  }
}
var catalogProducts = document.getElementsByClassName('catalog-product');

if (catalogProducts) {
  $(window).on('load',function () {
    for ( var i = 0; i < catalogProducts.length; i++ ) {
      catalogProducts[i].parentElement.style.height = getComputedStyle(catalogProducts[i]).height;
    }
  })

  $ ( window ) . resize (function () {
    for ( var i = 0; i < catalogProducts.length; i++ ) {
      catalogProducts[i].parentElement.style.height = getComputedStyle(catalogProducts[i]).height;
    }
  });
}

$('.fotorama').fotorama({
  width:'100%',
  ratio: '800/390',
  loop: true,
  arrows: true
});
$('.link-interactive').click(function(event) {
  $(this).modal({
    showClose: false
  });
  return false;
});


var menuProductsItem = document.querySelectorAll ('.navigation-mobile__item');
for ( var i = 0 ; i < menuProductsItem.length ; i++ ) {

  if ( menuProductsItem[i].querySelector ('.nav-submenu')) {
    menuProductsItem[i].classList.add('navigation-mobile__item_parent')
    var parentSwitcher = document.createElement('div');
    parentSwitcher.classList.add('navigation-mobile__parent-switcher')
    menuProductsItem[i].insertBefore(parentSwitcher, menuProductsItem[i].children[1])
  }
}
var parentSwitchers = document.querySelectorAll ('.navigation-mobile__parent-switcher');
for ( var i = 0 ; i < parentSwitchers.length ; i++ ) {
  parentSwitchers[i].addEventListener('click', function() {
    if ( !this.nextElementSibling.classList.contains( 'nav-submenu_display' ) ) {
      this.nextElementSibling.classList.add( 'nav-submenu_display' )
    } else {
      this.nextElementSibling.classList.remove( 'nav-submenu_display' )
    }
  })
}


//установка последнего дня текущего месяца для акции

var date = new Date();
var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
var months = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря']
var actionDate = document.getElementsByClassName('js-action-date');
for ( i = 0; i < actionDate.length; i++) {
  actionDate[i].innerText = lastDay.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear();
}

//поведение мобильного фильтра

var mobileFilterSelects = document.querySelectorAll('.mobile-filter-item__select')
function mobileFilterActive (event) {
  var currentOptions = [];
  var activeLabel = this.parentNode.querySelector('.mobile-filter-item__label_active');
  var inactiveLabel = this.parentNode.querySelector('.mobile-filter-item__label_inactive');
  for ( var j = 0 ; j < this.options.length ; j++ ) {
    var option = this.options[j];
    if (option.selected) {
      currentOptions.push(option.innerText);
    }
  }
  if (currentOptions.length > 0) {
    activeLabel.innerText = currentOptions.join(', ');
    inactiveLabel.style.display = 'none';
  } else {
    activeLabel.innerText = ''
    inactiveLabel.style.display = 'block';
  }
}
for ( var i = 0 ; i < mobileFilterSelects.length ; i++ ) {
  mobileFilterSelects[i].addEventListener ( 'focusout' , mobileFilterActive )
  mobileFilterSelects[i].addEventListener ( 'click' , mobileFilterActive )
  mobileFilterSelects[i].addEventListener ( 'change' , mobileFilterActive )
  
  
}


document.addEventListener("DOMContentLoaded", function () {
  for ( var i = 0 ; i < mobileFilterSelects.length ; i++ ) {
      var currentOptions = [];
  var activeLabel = mobileFilterSelects[i].parentNode.querySelector('.mobile-filter-item__label_active');
  var inactiveLabel = mobileFilterSelects[i].parentNode.querySelector('.mobile-filter-item__label_inactive');
  for ( var j = 0 ; j < mobileFilterSelects[i].options.length ; j++ ) {
    var option = mobileFilterSelects[i].options[j];
    if (option.selected) {
      currentOptions.push(option.innerText);
    }
  }
  if (currentOptions.length > 0) {
    activeLabel.innerText = currentOptions.join(', ');
    inactiveLabel.style.display = 'none';
  } else {
    activeLabel.innerText = ''
    inactiveLabel.style.display = 'block';
  }
  }
})

var headerMobile = document.querySelector( '.header-mobile' );

headerMobile.addEventListener( 'click', ( e ) => {
  let hmm = headerMobile.querySelector( '.header-mobile__menu' );
  let hmp = headerMobile.querySelector( '.header-mobile__phone' );
  let hms = headerMobile.querySelector( '.header-mobile__search' );
  let hmss = headerMobile.querySelector( '.header-mobile__search-switch' );
  let searchInput = hms.querySelector('.header-search__input');
  if ( e.target == hmss ) {
    hmm.classList.toggle( 'header-mobile__menu--inactive' );
    hmp.classList.toggle( 'header-mobile__phone--inactive' );
    hms.classList.toggle( 'header-mobile__search--active' );
    hmss.classList.toggle( 'header-mobile__search-switch--active' );
    if ( hms.classList.contains( 'header-mobile__search--active' ) ) {
      searchInput.focus()
    } else {
      searchInput.focus()
    }
  }
} )

const instaWidget = (settings = {element: ''}) => {

  class InstaWidget {
    constructor(settings = {
      elem: '',
      userId: '',
      title: '',
    }) {
      this.elem = settings.elem;

      this.userId = settings.userId || '';

      if (this.elem.dataset.title) this.title = this.elem.dataset.title;
      else this.title = settings.title;

      const url = settings.url || {};

      this.url = {
        getName: url.getName || '/instagram-widget/index.php?get_user_name=Y',
        getMedia: url.getMedia || '/instagram-widget/index.php',
        refreshToken: url.refreshToken || '/instagram-widget/index.php?refresh=Y',
      };
    }

    async userName() {
      const response = await fetch(this.url.getName);
      const user = await response.text();
      return user;
    }

    init() {
      new Promise((res) => {
        res(this.userName());
      })
        .then((res) => {
          this.userId = this.userId ? this.userId : res;
          this.title = this.title ? this.title : res;
        })
        .then(() => {
          if (this.elem.children.length > 0 || !this.url) {
            return;
          }
          const list = document.createElement('div');
          const title = document.createElement('div');
          const titleLink = document.createElement('a');
          const filter = this.elem.dataset.tagFilter ? (
            this.elem.dataset.tagFilter
              .split(',')
              .map((item) => `#${item.trim()}`)
          ) : [''];
          const className = 'instagram-widget';
          this.elem.classList.add(className);

          list.classList.add(`${className}__list`);
          title.classList.add(`${className}__title`);
          titleLink.setAttribute('href', `https://www.instagram.com/${this.userId}/`);
          titleLink.setAttribute('target', '_blank');
          titleLink.innerText = `${this.title}`;
          title.appendChild(titleLink);
          this.elem.appendChild(title);

          fetch(this.url.getMedia)
            .then((response) => response.json())
            .then((response) => response.data.filter((item) => filter.map((filterItem) => item.caption.includes(filterItem)).filter((i) => i === true).length > 0)) // eslint-disable-line max-len
            .then((result) => {
              const data = result.slice(0, 8);
              data.forEach((item) => {
                const inwidgetItem = document.createElement('div');
                const inwidgetItemLink = document.createElement('a');
                const inwidgetItemImage = document.createElement('img');
                inwidgetItem.className = `${className}__item`;
                inwidgetItemLink.href = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemLink.dataset.fancybox = 'inwidget';
                inwidgetItemImage.src = item.media_type === 'VIDEO' ? item.thumbnail_url : item.media_url;
                inwidgetItemImage.alt = item.caption;
                inwidgetItemLink.appendChild(inwidgetItemImage);
                inwidgetItem.appendChild(inwidgetItemLink);
                list.appendChild(inwidgetItem);
              });
              this.elem.appendChild(list);
            })
            .catch(() => {
              this.elem.remove();
            });
        });
    }
  }
  const selector = settings.element || '.instagram-widget';
  const inwidget = Array.from(document.querySelectorAll(selector));

  inwidget.forEach((item) => {
    const widget = new InstaWidget({ 'elem': item });
    widget.init();
  })
}

instaWidget();
