import gulp from 'gulp';

gulp.task('default', gulp.series('stylus', 'template', 'copy', 'server', 'watcher'));
gulp.task('build', gulp.parallel('stylus', 'template', 'copy'));
