import gulp from 'gulp';
import watch from 'gulp-watch';
import browserSync from 'browser-sync';

const { reload } = browserSync;

gulp.task('reload', (done) => {
  reload();
  done();
});

gulp.task('watcher', (done) => {
  global.isWatching = true;
  watch([
    './src/styles/*.styl',
    './src/styles/includes/*',
    './src/styles/svg/*.svg',
    './src/blocks/**/*.styl',
    './src/blocks/**/**/*.styl',
  ], gulp.series('stylus', 'reload'));
  watch([
    './src/blocks/**/*.pug',
    './src/blocks/**/**/*.pug',
    './src/pages/**/*.pug',
    './src/data/*',
  ], gulp.series('template', 'reload'));
  watch('./src/resources/**', gulp.series('copy:resources', 'reload'));
  watch('./src/template/**', gulp.series('copy:template', 'reload'));
  done();
});
