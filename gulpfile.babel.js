require('require-dir')('tasks', { recurse: true });

export const isDebug = process.env.NODE_ENV !== 'production';
export const distDir = './dist';
export const templateDir = `${distDir}/`;
